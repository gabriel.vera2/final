**FINAL Vera, Gabriel Hernán**

Lo realizado hasta el momento:

- Se creó la rama main y develop en este repositorio Git.
- En la rama "develop" se encuentra un README.md, y un index.html.
- En la rama "main" se encuentra un README.md completo, un index.html final y archivo nginx.conf

Si el usuario desea:

- Detallaremos cómo formatear un volumen "previamente disponible" en un directorio elegido.
- Lanzaremos un contenedor usando la imagen ubuntu/apache2 montando en el directorio /var/www/html con el directorio que previamente asociamos.
- Clonaremos este repositorio localmente en nuestra máquina virtual, y copiaremos el fichero index.html de la rama main al punto de montaje elegido en el paso 1.


**Pasos a realizar para el primer punto.**

Paso 1, examinar si se ve el disco "SDB":
```
$ lsblk
```
Paso 2, continuar con el formateo:
```
# sudo fdisk /dev/sdb
```
Paso 3, presionaremos la tecla "n" para nueva partición.

Paso 4, presionaremos la tecla "p" para partición primaria.

Paso 5, presionaremos el número "1" para seleccionar número de partición.

Paso 6, aceptar dos veces con "enter" y presionar la tecla "w" para finalizar.

Paso 7, continuar con comandos de sistema de archivos, y montaje:
```
# sudo mkfs.ext4 /dev/sdb1
```
```
# sudo mkdir /mnt/data
```
```
# sudo mount /dev/sdb1 /mnt/data
```
Paso 7, si queremos que inicie montado así:
```
# sudo nano /etc/fstab
```
Paso 8, agregar al final del archivo lo siguiente, luego guardar y salir:
```
/dev/sdb1 /mnt/data ext4 defaults 0 2
```

**Pasos a realizar para el segundo punto.**

Paso 1, instalar docker.io:
```
# sudo apt install docker.io -y 
```
Paso 2, lanzar contenedor con el directorio asociado al volumen:
```
# sudo docker run -d -p 80:80 --name apache_container -v /mnt/data:/var/www/html ubuntu/apache2
```
Paso 3, verificar que este corriendo:
```
# sudo docker ps
```

**Pasos para realizar el tercer punto.**

Paso 1, clonar este repositorio git a tu local:
```
$ git clone https://gitlab.com/gabriel.vera2/final.git
```
Paso 2, entrar al repositorio para copiar el archivo al punto de montaje:
```
$ cd final
```
```
# sudo cp index.html /mnt/data/
```

**Para el archivo NGINX**

Añadir esta configuración al servidor proxy reverso.

```
server {
    listen 80;
    server_name final.com;

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://ip_docker_server:80;
    }
}

```
> Nota: el archivo es el nginx.conf
